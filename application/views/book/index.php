
	<div class="row">
		<a href="<?php echo base_url('book/issue'); ?>" class="btn btn-primary">Issue Book</a>
		<a href="<?php echo base_url('book/availability'); ?>" class="btn btn-primary">Book Availability</a>
	</div>

	<h3>Issued Books</h3>

	<?php
		if($this->session->flashdata('success_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
	<?php		
		}
	?>


	<?php
		if($this->session->flashdata('error_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('error_msg'); ?>
		</div>
	<?php		
		}
	?>

	<div>
		<table class="table table-bordered table-responsive">
			<thead>
				<tr>
					<td>Sr No</td>
					<th>User</th>
					<th>Book isbn</th>
					<th>Book Name</th>
					<th>Author</th>
					<th>Issued On</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				if($books){
					$i = 0;
					foreach($books as $book){
					$i = $i+1;

			?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $book->name; ?></td>
					<td><?php echo $book->isbn; ?></td>
					<td><?php echo $book->title; ?></td>
					<td><?php echo $book->author; ?></td>
					<td><?php echo $book->issued_on; ?></td>
					<td><?php echo $book->phone; ?></td>
					<td><?php echo $book->email ?></td>
					<td>
						<a href="<?php echo base_url('book/returnBook/'.$book->id); ?>" class="btn btn-info">Return</a>
					</td>
				</tr>
			<?php
					}
				}
			?>
			</tbody>
		</table>
	</div>