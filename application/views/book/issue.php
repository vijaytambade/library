
<h3>Issue Book</h3>
<a href="<?php echo base_url('book/index'); ?>" class="btn btn-default">Back</a>

<?php
	if($this->session->flashdata('error_msg')){
?>
	<div class="alert alert-success">
		<?php echo $this->session->flashdata('error_msg'); ?>
	</div>
<?php		
	}
?>
	
<form action="<?php echo base_url('book/submit') ?>" method="post" class="form-horizontal">
	<div class="form-group">
		<label for="title" class="col-md-4 text-right">User</label>
		<div class="col-md-4">
		<select class="form-control" id="sel_user" name="user" required>
		<option>Select User</option>
			<?php 
				if($users){
					foreach($users as $user){
						echo '<option value="'.$user->user_id.'">'.$user->name.'</option>';
					}
				}
			?>		    
		</select>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="form-group">
		<label for="title" class="col-md-4 text-right">Book</label>
		<div class="col-md-4">
		<select class="form-control" id="sel_book" name="book" required>
			<option value="0">Select Book</option>
			<?php 
				if($books){
					foreach($books as $book){
						echo '<option value="'.$book->book_id.'">'.$book->title.'</option>';
					}
				}
			?>
		</select>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="form-group">
		<label class="col-md-4 text-right"></label>
		<div class="col-md-6">
			<input type="submit" name="btnSave" class="btn btn-primary" value="Save">
		</div>
	</div>
</form>
	
