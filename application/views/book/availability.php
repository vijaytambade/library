
	<h3>Available  Book list</h3><br>
	<a href="<?php echo base_url('book/index'); ?>" class="btn btn-default">Back</a>
	<div>
		<table class="table table-bordered table-responsive">
			<thead>
				<tr>
					<td>Sr No</td>
					<th>Book isbn</th>
					<th>Book Name</th>
					<th>Author</th>
					<th>Total Quantity</th>
					<th>Available Quantity</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				if($books){
					$i = 0;
					foreach($books as $book){
					$i = $i+1;
			?>
				<tr>
					<td><?php echo $i++; ?></td>	
					<td><?php echo $book->isbn; ?></td>
					<td><?php echo $book->title; ?></td>
					<td><?php echo $book->author; ?></td>
					<td><?php echo $book->total_quantity; ?></td>
					<td><?php echo $book->available_quantity; ?></td>
				</tr>
			<?php
					}
				}
			?>
			</tbody>
		</table>
	</div>