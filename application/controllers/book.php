<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** Book Controller
	Author: Vijay T
	Path: /book
*/
class Book extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('book_m', 'm');
		$this->load->model('user_m', 'user');
	}

	/**
		View Issued book list
		path: /book
	*/
	function index(){
		$data['books'] = $this->m->getBookIssuedList();
		$this->load->view('layout/header');
		$this->load->view('book/index', $data);
		$this->load->view('layout/footer');
	}

	/**
		Issue new book to user
		path: /book/issue
	*/
	public function issue(){
		// fetch available books from db
		$data['users'] = $this->user->getUsers();
		// fetch available users from db
		$data['books'] = $this->m->getAvailableBooks();
		$this->load->view('layout/header');
		$this->load->view('book/issue', $data);
		$this->load->view('layout/footer');
	}

	/**
		Show in stocks book list
		path: /book/availability
	*/
	public function availability(){
		$data['books'] = $this->m->getAvailableBooks();
		$this->load->view('layout/header');
		$this->load->view('book/availability', $data);
		$this->load->view('layout/footer');
	}

	/**
		Submit book issue request
		path: /book/submit
		param: book - issued book id, user - user id
	*/
	public function submit(){

		if($this->input->post('book') == "0" || $this->input->post('user') == "0"){
			$this->session->set_flashdata('error_msg', 'Please Select all values');
			redirect(base_url('book/issue'));
		}

		$result = $this->m->issueBook();
		if($result){
			$this->session->set_flashdata('success_msg', 'Book issued successfully');
		}else{
			$this->session->set_flashdata('error_msg', 'Faill to issue book');
		}
		redirect(base_url('book/index'));
	}

	/**
		Return issued book
		path: /book/returnBook
		param: book - issued user-book association id
	*/
	public function returnBook($id){
		$result = $this->m->returnBook($id);
		if($result){
			$this->session->set_flashdata('success_msg', 'Book submitted successfully');
		}else{
			$this->session->set_flashdata('error_msg', 'Faill to submit book');
		}
		redirect(base_url('book/index'));
	}

}