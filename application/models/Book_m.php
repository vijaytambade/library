<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** Book Model
	Author: Vijay T
*/
class Book_m extends CI_Model{

	// fetch list of issued book-user list
	public function getBookIssuedList(){
		$this->db->select('book.isbn, book.title, book.author, 
										user.name, user.phone, user.email,	
										book_user_association.id, book_user_association.issued_on');
		$this->db->from('book_user_association');
		$this->db->join('book','book.book_id=book_user_association.book_id');
		$this->db->join('user','user.user_id=book_user_association.user_id');
		$this->db->where('book_user_association.status', 0);

		$query=$this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

 // insert record of book issue
	public function issueBook(){
		print_r($this->input->post());
		$field = array(
			'book_id'=>$this->input->post('book'),
			'user_id'=>$this->input->post('user'),
			'issued_on'=>date('Y-m-d H:i:s'),
			'status'=>0
			);

		$this->db->insert('book_user_association', $field);
		if($this->db->affected_rows() > 0){
			// Decrese book available count
			$this->db->where('book_id', $this->input->post('book'));
			$this->db->set('available_quantity', 'available_quantity-1', FALSE);
			$this->db->update('book');
			return true;

		}else{
			return false;
		}
	}

	// insert book return record
	public function returnBook($id){
		$field = array(
			'returned_on'=>date('Y-m-d H:i:s'), 
			'status' => 1
			);
		$this->db->where('id', $id);
		$this->db->update('book_user_association', $field);
		echo $this->db->last_query();
		if($this->db->affected_rows() > 0){
			// Increase book available count
			$this->db->where('book_id', $this->input->post('book'));
			$this->db->set('available_quantity', 'available_quantity+1', FALSE);
			$this->db->update('book');
			return true;
		}else{
			return false;
		}
	}

	// fetch available books from stock
	public function getAvailableBooks(){
		$this->db->where('available_quantity >', '0');
		$query = $this->db->get('book');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

}