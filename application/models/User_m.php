<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** User Model
	Author: Vijay T
*/
class User_m extends CI_Model{

	// fetch user list from Db
	public function getUsers(){
		$query = $this->db->get('user');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

}