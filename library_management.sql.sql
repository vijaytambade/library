-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2019 at 12:32 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `book_id` int(11) NOT NULL,
  `isbn` varchar(200) COLLATE utf16_bin NOT NULL,
  `title` varchar(500) COLLATE utf16_bin NOT NULL,
  `author` varchar(200) COLLATE utf16_bin NOT NULL,
  `total_quantity` int(11) NOT NULL DEFAULT '0',
  `available_quantity` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`book_id`, `isbn`, `title`, `author`, `total_quantity`, `available_quantity`) VALUES
(1, 'isbn_1', 'PHP in Depth', 'Author', 10, 7),
(2, 'isbn_2', 'Games of Thrones', 'sdjndnnmndsmn', 40, 38);

-- --------------------------------------------------------

--
-- Table structure for table `book_user_association`
--

CREATE TABLE `book_user_association` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `issued_on` datetime DEFAULT NULL,
  `returned_on` datetime DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `book_user_association`
--

INSERT INTO `book_user_association` (`id`, `book_id`, `user_id`, `issued_on`, `returned_on`, `status`) VALUES
(1, 1, 1, '2019-11-08 07:00:18', '0000-00-00 00:00:00', 0),
(2, 2, 1, '2019-11-08 18:24:02', '2019-11-08 18:33:58', 1),
(4, 2, 2, '2019-11-08 18:35:08', NULL, 0),
(5, 2, 1, '2019-11-08 18:35:48', '2019-11-08 18:35:51', 1),
(6, 1, 2, '2019-11-08 18:42:18', '2019-11-08 18:53:23', 1),
(11, 2, 3, '2019-11-08 18:53:14', NULL, 0),
(12, 1, 2, '2019-11-08 19:09:42', '2019-11-09 12:24:30', 1),
(13, 2, 1, '2019-11-09 12:24:58', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf16_bin NOT NULL,
  `email` varchar(100) COLLATE utf16_bin NOT NULL,
  `phone` varchar(50) COLLATE utf16_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `email`, `phone`) VALUES
(1, 'Vijay', 'Vijay@test@gmail.com', '1234567890'),
(2, 'Sam', 'sam@test.lcl', '1234567890'),
(3, 'Jon', 'jon@djfkjd.ll', '1234567890');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `book_user_association`
--
ALTER TABLE `book_user_association`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_book_id` (`book_id`),
  ADD KEY `fk_user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `book_user_association`
--
ALTER TABLE `book_user_association`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `book_user_association`
--
ALTER TABLE `book_user_association`
  ADD CONSTRAINT `fk_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`),
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
